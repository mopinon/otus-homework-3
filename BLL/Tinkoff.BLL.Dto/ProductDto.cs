namespace Tinkoff.BLL.Dto;

public class ProductDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int OwnerId { get; set; }
}