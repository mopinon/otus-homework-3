namespace Tinkoff.BLL.Dto;

public class GenerateValueDto
{
    public IEnumerable<UserDto> Users { get; set; }
    public IEnumerable<ProductDto> Products { get; set; }
    public IEnumerable<IndividualEntrepreneurDto> IndividualEntrepreneurs { get; set; }
    public IEnumerable<PromotionDto> Promotions { get; set; }
    public IEnumerable<PromotionUserDto> PromotionUsers { get; set; }
}