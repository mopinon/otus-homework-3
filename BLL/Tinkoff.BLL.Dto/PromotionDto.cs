namespace Tinkoff.BLL.Dto;

public class PromotionDto
{
    public int Id { get; set; }
    public string Name { get; set; }
}