namespace Tinkoff.BLL.Dto;

public class AddProductDto
{
    public string Name { get; set; }
    public int OwnerId { get; set; }
}