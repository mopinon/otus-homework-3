namespace Tinkoff.BLL.Dto;

public class UserDto
{
    public int Id { get; set; }
    public string Email { get; set; }
    public string TelephoneNumder { get; set; }
}