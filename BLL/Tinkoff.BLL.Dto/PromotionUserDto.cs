namespace Tinkoff.BLL.Dto;

public class PromotionUserDto
{
    public int MemberId { get; set; }
    public int PromotionId { get; set; }
}