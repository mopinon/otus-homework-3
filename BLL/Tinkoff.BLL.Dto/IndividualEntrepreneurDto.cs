namespace Tinkoff.BLL.Dto;

public class IndividualEntrepreneurDto
{
    public int Id { get; set; }
    public int UserId { get; set; }
}