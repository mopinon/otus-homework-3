using Tinkoff.BLL.Dto;

namespace Tinkoff.BLL.Contracts;

public interface IProductService
{
    public Task<IEnumerable<ProductDto>> GenerateValueAsync();
    public Task<AddProductDto> AddAsync(AddProductDto productDto);
    public Task<IEnumerable<ProductDto>> GetAllAsync();
}