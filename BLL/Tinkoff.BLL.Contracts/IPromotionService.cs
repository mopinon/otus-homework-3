using Tinkoff.BLL.Dto;

namespace Tinkoff.BLL.Contracts;

public interface IPromotionService
{
    public Task<IEnumerable<PromotionDto>> GenerateValueAsync();
    public Task<IEnumerable<PromotionDto>> GetAllAsync();
}