using Tinkoff.BLL.Dto;

namespace Tinkoff.BLL.Contracts;

public interface IIndividualEntrepreneurService
{
    public Task<IEnumerable<IndividualEntrepreneurDto>> GenerateValueAsync();
    public Task<IEnumerable<IndividualEntrepreneurDto>> GetAllAsync();
}