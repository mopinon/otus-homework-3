using Tinkoff.BLL.Dto;

namespace Tinkoff.BLL.Contracts;

public interface IUserService
{
    public Task<IEnumerable<UserDto>> GenerateValueAsync();
    public Task<IEnumerable<UserDto>> GetAllAsync();
}