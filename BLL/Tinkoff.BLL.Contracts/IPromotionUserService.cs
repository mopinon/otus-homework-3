using Tinkoff.BLL.Dto;

namespace Tinkoff.BLL.Contracts;

public interface IPromotionUserService
{
    public Task<IEnumerable<PromotionUserDto>> GenerateValueAsync();
    public Task<IEnumerable<PromotionUserDto>> GetAllAsync();
}