using AutoMapper;
using Tinkoff.BLL.Contracts;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Entities;

namespace Tinkoff.BLL;

public class PromotionUserService : IPromotionUserService
{
    private readonly IPromotionUserRepository _promotionUserRepository;
    private readonly IMapper _mapper;

    public PromotionUserService(IPromotionUserRepository promotionUserRepository, IMapper mapper)
    {
        _promotionUserRepository = promotionUserRepository;
        _mapper = mapper;
    }
    
    public async Task<IEnumerable<PromotionUserDto>> GenerateValueAsync()
    {
        var result = new List<PromotionUserDto>();
        
        for (var i = 1; i <= 5; i++)
        {
            var promotionUser = new PromotionUser
            {
                MemberId = new Random().Next(1, 5),
                PromotionId = new Random().Next(1, 5)
            };

            await _promotionUserRepository.AddAsync(promotionUser);
            
            var generatePromotionDto = _mapper.Map<PromotionUserDto>(promotionUser);
            result.Add(generatePromotionDto);
        }
        
        return result;
    }

    public async Task<IEnumerable<PromotionUserDto>> GetAllAsync()
    {
        var promotionUsers = await _promotionUserRepository.ListAllAsync();
        var promotionUserDtos = promotionUsers.Select(e => _mapper.Map<PromotionUserDto>(e));

        return promotionUserDtos;
    }
}