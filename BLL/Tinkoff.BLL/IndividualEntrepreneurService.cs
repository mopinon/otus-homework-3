using AutoMapper;
using Tinkoff.BLL.Contracts;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Entities;

namespace Tinkoff.BLL;

public class IndividualEntrepreneurService : IIndividualEntrepreneurService
{
    private readonly IIndividualEntrepreneurRepository _individualEntrepreneurRepository;
    private readonly IMapper _mapper;

    public IndividualEntrepreneurService(IIndividualEntrepreneurRepository individualEntrepreneurRepository,
        IMapper mapper)
    {
        _individualEntrepreneurRepository = individualEntrepreneurRepository;
        _mapper = mapper;
    }

    public async Task<IEnumerable<IndividualEntrepreneurDto>> GenerateValueAsync()
    {
        var result = new List<IndividualEntrepreneurDto>();

        for (var i = 1; i <= 5; i++)
        {
            var individualEntrepreneur = new IndividualEntrepreneur
            {
                UserId = i
            };

            await _individualEntrepreneurRepository.AddAsync(individualEntrepreneur);

            var generateUserDto = _mapper.Map<IndividualEntrepreneurDto>(individualEntrepreneur);
            result.Add(generateUserDto);
        }

        return result;
    }

    public async Task<IEnumerable<IndividualEntrepreneurDto>> GetAllAsync()
    {
        var individualEntrepreneurs = await _individualEntrepreneurRepository.ListAllAsync();
        var individualEntrepreneurDtos = individualEntrepreneurs.Select(e => _mapper.Map<IndividualEntrepreneurDto>(e));

        return individualEntrepreneurDtos;    
    }
}