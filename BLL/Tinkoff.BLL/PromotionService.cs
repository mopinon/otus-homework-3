using AutoMapper;
using Tinkoff.BLL.Contracts;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Entities;

namespace Tinkoff.BLL;

public class PromotionService : IPromotionService
{
    private readonly IPromotionRepository _promotionRepository;
    private readonly IMapper _mapper;

    public PromotionService(IPromotionRepository promotionRepository, IMapper mapper)
    {
        _promotionRepository = promotionRepository;
        _mapper = mapper;
    }
    
    public async Task<IEnumerable<PromotionDto>> GenerateValueAsync()
    {
        var result = new List<PromotionDto>();
        
        for (var i = 1; i <= 5; i++)
        {
            var promotion = new Promotion
            {
                Name = $"{i}{i}{i}_Promotion",
            };

            await _promotionRepository.AddAsync(promotion);
            
            var generatePromotionDto = _mapper.Map<PromotionDto>(promotion);
            result.Add(generatePromotionDto);
        }
        
        return result;
    }

    public async Task<IEnumerable<PromotionDto>> GetAllAsync()
    {
        var promotions = await _promotionRepository.ListAllAsync();
        var promotionDtos = promotions.Select(e => _mapper.Map<PromotionDto>(e));

        return promotionDtos;
    }
}