using AutoMapper;
using Tinkoff.BLL.Contracts;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Entities;

namespace Tinkoff.BLL;

public class ProductService : IProductService
{
    private readonly IProductRepository _productRepository;
    private readonly IUserRepository _userRepository;
    private readonly IMapper _mapper;

    public ProductService(IProductRepository productRepository, IMapper mapper, IUserRepository userRepository)
    {
        _productRepository = productRepository;
        _mapper = mapper;
        _userRepository = userRepository;
    }
    
    public async Task<IEnumerable<ProductDto>> GenerateValueAsync()
    {
        var result = new List<ProductDto>();
        
        for (var i = 1; i <= 5; i++)
        {
            var product = new Product
            {
                Name = $"{i}{i}{i}_Product",
                OwnerId = new Random().Next(1, 5)
            };

            await _productRepository.AddAsync(product);
            
            var generateProductDto = _mapper.Map<ProductDto>(product);
            result.Add(generateProductDto);
        }
        
        return result;
    }

    public async Task<AddProductDto> AddAsync(AddProductDto productDto)
    {
        var user = await _userRepository.GetByIdAsync(productDto.OwnerId);
        if (user is null)
            throw new InvalidOperationException("invalid user id");

        var product = _mapper.Map<Product>(productDto);
        await _productRepository.AddAsync(product);
        
        return productDto;
    }

    public async Task<IEnumerable<ProductDto>> GetAllAsync()
    {
        var products = await _productRepository.ListAllAsync();
        var productDtos = products.Select(e => _mapper.Map<ProductDto>(e));

        return productDtos;
    }
}