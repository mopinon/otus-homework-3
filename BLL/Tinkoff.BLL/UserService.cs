using AutoMapper;
using Tinkoff.BLL.Contracts;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Entities;

namespace Tinkoff.BLL;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IMapper _mapper;

    public UserService(IUserRepository userRepository, IMapper mapper)
    {
        _userRepository = userRepository;
        _mapper = mapper;
    }
    
    public async Task<IEnumerable<UserDto>> GenerateValueAsync()
    {
        var result = new List<UserDto>();
        
        for (var i = 1; i <= 5; i++)
        {
            var user = new User
            {
                Email = $"{i}@mail.ru",
                TelephoneNumder = $"+7({i}{i}{i}){i}{i}{i}-{i}{i}-{i}{i}"
            };

            await _userRepository.AddAsync(user);
            
            var generateUserDto = _mapper.Map<UserDto>(user);
            result.Add(generateUserDto);
        }
        
        return result;
    }

    public async Task<IEnumerable<UserDto>> GetAllAsync()
    {
        var users = await _userRepository.ListAllAsync();
        var userDtos = users.Select(e => _mapper.Map<UserDto>(e));

        return userDtos;
    }
}