using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL.Contracts;

public interface IPromotionUserRepository : IRepository<PromotionUser, int>, IAsyncRepository<PromotionUser, int>
{
}