using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL.Contracts;

public interface IPromotionRepository : IRepository<Promotion, int>, IAsyncRepository<Promotion, int>
{
}