using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL.Contracts;

public interface IUserRepository : IRepository<User, int>, IAsyncRepository<User, int>
{
}