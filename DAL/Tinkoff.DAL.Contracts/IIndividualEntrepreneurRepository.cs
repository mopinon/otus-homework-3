using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL.Contracts;

public interface IIndividualEntrepreneurRepository : IRepository<IndividualEntrepreneur, int>,
    IAsyncRepository<IndividualEntrepreneur, int>
{
}