using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL.Contracts;

public interface IProductRepository : IRepository<Product, int>, IAsyncRepository<Product, int>
{
}