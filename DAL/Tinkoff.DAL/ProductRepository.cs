using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL;

public class ProductRepository : BaseRepository<Product, int>, IProductRepository
{
    public ProductRepository(ApplicationContext dbContext) : base(dbContext)
    {
    }
}