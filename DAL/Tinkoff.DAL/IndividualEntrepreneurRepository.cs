using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL;

public class IndividualEntrepreneurRepository : BaseRepository<IndividualEntrepreneur, int>,
    IIndividualEntrepreneurRepository
{
    public IndividualEntrepreneurRepository(ApplicationContext dbContext) : base(dbContext)
    {
    }
}