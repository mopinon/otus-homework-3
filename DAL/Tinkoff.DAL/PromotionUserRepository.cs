using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL;

public class PromotionUserRepository : BaseRepository<PromotionUser, int>, IPromotionUserRepository
{
    public PromotionUserRepository(ApplicationContext dbContext) : base(dbContext)
    {
    }
}