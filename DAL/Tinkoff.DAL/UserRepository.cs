using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL;

public class UserRepository : BaseRepository<User, int>, IUserRepository
{
    public UserRepository(ApplicationContext dbContext) : base(dbContext)
    {
    }
}