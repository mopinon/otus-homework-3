using Tinkoff.DAL.Contracts;
using Tinkoff.DAL.Core;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL;

public class PromotionRepository : BaseRepository<Promotion, int>, IPromotionRepository
{
    public PromotionRepository(ApplicationContext dbContext) : base(dbContext)
    {
    }
}