namespace Tinkoff.DAL.Entities;

public class Promotion
{
    public int Id { get; set; }
    public string Name { get; set; }
    // many to many
    public ICollection<User> Members { get; set; }
    public ICollection<PromotionUser> PromotionUsers { get; set; }
}