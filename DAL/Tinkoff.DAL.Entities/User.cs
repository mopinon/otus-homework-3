using System.ComponentModel.DataAnnotations;

namespace Tinkoff.DAL.Entities;

public class User
{
    public int Id { get; set; }
    [Required]
    public string Email { get; set; }
    [Required]
    public string TelephoneNumder { get; set; }
    // one to many
    public ICollection<Product> Products { get; set; }
    // many to many
    public ICollection<Promotion> Promotions { get; set; }
    public ICollection<PromotionUser> PromotionUsers { get; set; }
    // one to one
    public IndividualEntrepreneur IndividualEntrepreneur { get; set; }
}