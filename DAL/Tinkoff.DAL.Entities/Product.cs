namespace Tinkoff.DAL.Entities;

public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    // one to many
    public User Owner { get; set; }
    public int OwnerId { get; set; }
}