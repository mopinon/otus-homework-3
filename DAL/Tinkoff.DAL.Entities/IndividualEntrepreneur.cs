namespace Tinkoff.DAL.Entities;

public class IndividualEntrepreneur
{
    public int Id { get; set; }
    // one-to-one
    public User User { get; set; }
    public int UserId { get; set; }
}