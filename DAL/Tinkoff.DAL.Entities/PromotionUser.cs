namespace Tinkoff.DAL.Entities;

public class PromotionUser
{
    public int MemberId { get; set; }
    public User Member { get; set; }
    public int PromotionId { get; set; } 
    public Promotion Promotion { get; set; }
}