using Microsoft.EntityFrameworkCore;
using Tinkoff.DAL.Entities;

namespace Tinkoff.DAL.Core;

public class ApplicationContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Promotion> Promotions { get; set; }
    public DbSet<IndividualEntrepreneur> IndividualEntrepreneurs { get; set; }
    public DbSet<PromotionUser> PromotionUsers { get; set; }

    public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<Promotion>()
            .HasMany(c => c.Members)
            .WithMany(s => s.Promotions)
            .UsingEntity<PromotionUser>(
                j => j
                    .HasOne(pt => pt.Member)
                    .WithMany(t => t.PromotionUsers)
                    .HasForeignKey(pt => pt.MemberId),
                j => j
                    .HasOne(pt => pt.Promotion)
                    .WithMany(p => p.PromotionUsers)
                    .HasForeignKey(pt => pt.PromotionId)
    
    // j =>
                // {
                //     j.HasKey(t => new {t.MemberId, t.PromotionId});
                //     j.ToTable("PromotionUser");
                );

    }
}