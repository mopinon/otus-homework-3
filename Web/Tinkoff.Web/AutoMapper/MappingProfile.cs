using AutoMapper;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Entities;

namespace Tinkoff.AutoMapper;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<User, UserDto>().ReverseMap();
        CreateMap<Product, ProductDto>().ReverseMap();
        CreateMap<Promotion, PromotionDto>().ReverseMap();
        CreateMap<PromotionUser, PromotionUserDto>().ReverseMap();
        CreateMap<IndividualEntrepreneur, IndividualEntrepreneurDto>().ReverseMap();
        CreateMap<Product, AddProductDto>().ReverseMap();
    }
}