using Microsoft.AspNetCore.Mvc;
using Tinkoff.BLL.Contracts;
using Tinkoff.BLL.Dto;
using Tinkoff.DAL.Core;

namespace Tinkoff.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class AppController : Controller
{
    private readonly IUserService _userService;
    private readonly IProductService _productService;
    private readonly IIndividualEntrepreneurService _individualEntrepreneurService;
    private readonly IPromotionService _promotionService;
    private readonly IPromotionUserService _promotionUserService;
    
    private readonly ApplicationContext _applicationContext; // это находится здесь ради исключительного случая

    public AppController(
        IUserService userService,
        IProductService productService,
        IIndividualEntrepreneurService individualEntrepreneurService,
        IPromotionService promotionService,
        ApplicationContext applicationContext, 
        IPromotionUserService promotionUserService)
    {
        _userService = userService;
        _productService = productService;
        _individualEntrepreneurService = individualEntrepreneurService;
        _promotionService = promotionService;
        
        _applicationContext = applicationContext;
        _promotionUserService = promotionUserService;
    }

    [HttpGet]
    public async Task<ActionResult<GenerateValueDto>> FillDatabase()
    {
        _applicationContext.Database.EnsureDeleted(); // исключительный случай
        _applicationContext.Database.EnsureCreated();

        var response = new GenerateValueDto();

        var generatedUsers = await _userService.GenerateValueAsync();
        response.Users = generatedUsers;

        var generatedProducts = await _productService.GenerateValueAsync();
        response.Products = generatedProducts;

        var generatedIndividualEntrepreneurs = await _individualEntrepreneurService.GenerateValueAsync();
        response.IndividualEntrepreneurs = generatedIndividualEntrepreneurs;
        
        var generatedPromotions = await _promotionService.GenerateValueAsync();
        response.Promotions = generatedPromotions;
        
        var generatedPromotionUsers = await _promotionUserService.GenerateValueAsync();
        response.PromotionUsers = generatedPromotionUsers;

        return Ok(response);
    }

    [HttpPost]
    public async Task<ActionResult<AddProductDto>> AddProduct(AddProductDto request)
    {
        try
        {
            var response = await _productService.AddAsync(request);
            return new JsonResult(response);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpGet]
    public async Task<ActionResult<GetDataDto>> GetData()
    {
        var response = new GenerateValueDto();

        var generatedUsers = await _userService.GetAllAsync();
        response.Users = generatedUsers;

        var generatedProducts = await _productService.GetAllAsync();
        response.Products = generatedProducts;

        var generatedIndividualEntrepreneurs = await _individualEntrepreneurService.GetAllAsync();
        response.IndividualEntrepreneurs = generatedIndividualEntrepreneurs;
        
        var generatedPromotions = await _promotionService.GetAllAsync();
        response.Promotions = generatedPromotions;
        
        var generatedPromotionUsers = await _promotionUserService.GetAllAsync();
        response.PromotionUsers = generatedPromotionUsers;

        return Ok(response);
    }
}